$(document).ready(function () {
    $("body").queryLoader2({
        barColor: "#064cbe",
        backgroundColor: "#132033",
        percentage: true,
        barHeight: 3,
        minimumTime: 200,
        fadeOutTime: 1000
    });
    AOS.init({
        duration: 800,
        delay: 100
    });
    /*$(".getting-started").countdown('2018/05/01 12:00:00', function (event) {
        var $this = $(this).html(event.strftime('' + '<div class="date-number">%D <div class="date-title">Days</div></div>:' + '<div class="date-number">%H <div class="date-title">Hours</div></div>:' + '<div class="date-number">%M <div class="date-title">Minutes</div></div>:' + '<div class="date-number">%S <div class="date-title">Seconds</div></div>'));
    });*/
    $('.header-menu').on('click', function () {
        $(this).toggleClass('open');
        //$('#menu').toggleClass('open').fadeToggle();
        $('body').toggleClass('menu-open');
    });
    function onScroll(e) {
        var scrollPos = $(document).scrollTop();
        if (scrollPos > 0) {
            if (!$('header').hasClass('scroll')) $('header').addClass('scroll');
        } else {
            if ($('header').hasClass('scroll')) $('header').removeClass('scroll');
        }
    }

    onScroll();
    $(document).on("scroll", onScroll);

    //$('.header').midnight();

    $(document).on('click', '[data-dismiss^=modal]', function() {
        if ($('body').hasClass('menu-open'))  $('body').removeClass('menu-open');
        if ($('.header-menu').hasClass('open'))  $('.header-menu').removeClass('open');
    });

    $(document).on('click', '.close', function() {
        $(".modal-video iframe").each(function () {
            $(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
        });
    });
    $(document).on('click', '.toggle-open', function() {
        $(this).toggleClass('open').next($(this).data('toggle-next')).slideToggle();
    });

    /*var marquee = $(".currencies-scroll");
    var marqueeWidth = marquee.width()*2;
    marquee.css({"overflow": "hidden", "width": "100%"});
    marquee.wrapInner("<span class='currencies-line'>");
    marquee.find(".currencies-line").css({ "width": "50%", "display": "inline-block", "text-align":"center" });
    marquee.append(marquee.find(".currencies-line").clone()); // тут у нас два span с текстом
    marquee.wrapInner("<div class='currencies-content'>");
    marquee.find(".currencies-content").css("width", marqueeWidth);
    var reset = function() {
        $(this).css("margin-left", "0%");
        $(this).animate({ "margin-left": "-100%" }, 12000, 'linear', reset);
    };
    reset.call(marquee.find(".currencies-content"));*/

    /*$('.autoplay').slick({
        slidesToShow: 7,
        slidesToScroll: 1,
        autoplay: true,
        arrows:false,
        autoplaySpeed: 0,
        infinite: true,
        speed: 5000,
        cssEase: 'linear',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 5
          }
        },
        {
          breakpoint: 520,
          settings: {
            slidesToShow: 3
          }
        }

      ]
    });*/

    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.querySelector( 'span' ).innerHTML = "<span class='selected'>"+fileName+"</span>";
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });

});
